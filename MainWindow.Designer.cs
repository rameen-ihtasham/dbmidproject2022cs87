﻿namespace DBMidproject
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SideBarPanel = new Guna.UI2.WinForms.Guna2Panel();
            this.ManageEvaluationsBtn = new Guna.UI2.WinForms.Guna2Button();
            this.GroupProjectsBtn = new Guna.UI2.WinForms.Guna2Button();
            this.ManageGroups = new Guna.UI2.WinForms.Guna2Button();
            this.ManageProjectsBtn = new Guna.UI2.WinForms.Guna2Button();
            this.ManageAdvisorsBtn = new Guna.UI2.WinForms.Guna2Button();
            this.ManageStudentsBtn = new Guna.UI2.WinForms.Guna2Button();
            this.LogoPanel = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.HeaderPanel = new Guna.UI2.WinForms.Guna2Panel();
            this.ProjectNameLabel = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.MainScreenPanel = new Guna.UI2.WinForms.Guna2Panel();
            this.ReportsBtn = new Guna.UI2.WinForms.Guna2Button();
            this.SideBarPanel.SuspendLayout();
            this.LogoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.HeaderPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // SideBarPanel
            // 
            this.SideBarPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(15)))), ((int)(((byte)(43)))));
            this.SideBarPanel.Controls.Add(this.ReportsBtn);
            this.SideBarPanel.Controls.Add(this.ManageEvaluationsBtn);
            this.SideBarPanel.Controls.Add(this.GroupProjectsBtn);
            this.SideBarPanel.Controls.Add(this.ManageGroups);
            this.SideBarPanel.Controls.Add(this.ManageProjectsBtn);
            this.SideBarPanel.Controls.Add(this.ManageAdvisorsBtn);
            this.SideBarPanel.Controls.Add(this.ManageStudentsBtn);
            this.SideBarPanel.Controls.Add(this.LogoPanel);
            this.SideBarPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.SideBarPanel.Location = new System.Drawing.Point(0, 0);
            this.SideBarPanel.Margin = new System.Windows.Forms.Padding(0);
            this.SideBarPanel.Name = "SideBarPanel";
            this.SideBarPanel.Size = new System.Drawing.Size(219, 497);
            this.SideBarPanel.TabIndex = 0;
            // 
            // ManageEvaluationsBtn
            // 
            this.ManageEvaluationsBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(144)))), ((int)(((byte)(195)))));
            this.ManageEvaluationsBtn.BorderThickness = 1;
            this.ManageEvaluationsBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ManageEvaluationsBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ManageEvaluationsBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ManageEvaluationsBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ManageEvaluationsBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.ManageEvaluationsBtn.FillColor = System.Drawing.Color.Transparent;
            this.ManageEvaluationsBtn.Font = new System.Drawing.Font("Sitka Small", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManageEvaluationsBtn.ForeColor = System.Drawing.Color.White;
            this.ManageEvaluationsBtn.Location = new System.Drawing.Point(0, 396);
            this.ManageEvaluationsBtn.Name = "ManageEvaluationsBtn";
            this.ManageEvaluationsBtn.Size = new System.Drawing.Size(219, 45);
            this.ManageEvaluationsBtn.TabIndex = 5;
            this.ManageEvaluationsBtn.Text = "Manage Evaluations";
            this.ManageEvaluationsBtn.Click += new System.EventHandler(this.ManageEvaluationsBtn_Click);
            // 
            // GroupProjectsBtn
            // 
            this.GroupProjectsBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(144)))), ((int)(((byte)(195)))));
            this.GroupProjectsBtn.BorderThickness = 1;
            this.GroupProjectsBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.GroupProjectsBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.GroupProjectsBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.GroupProjectsBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.GroupProjectsBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupProjectsBtn.FillColor = System.Drawing.Color.Transparent;
            this.GroupProjectsBtn.Font = new System.Drawing.Font("Sitka Small", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupProjectsBtn.ForeColor = System.Drawing.Color.White;
            this.GroupProjectsBtn.Location = new System.Drawing.Point(0, 351);
            this.GroupProjectsBtn.Name = "GroupProjectsBtn";
            this.GroupProjectsBtn.Size = new System.Drawing.Size(219, 45);
            this.GroupProjectsBtn.TabIndex = 4;
            this.GroupProjectsBtn.Text = "Assign Projects";
            this.GroupProjectsBtn.Click += new System.EventHandler(this.GroupProjectsBtn_Click);
            // 
            // ManageGroups
            // 
            this.ManageGroups.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(144)))), ((int)(((byte)(195)))));
            this.ManageGroups.BorderThickness = 1;
            this.ManageGroups.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ManageGroups.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ManageGroups.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ManageGroups.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ManageGroups.Dock = System.Windows.Forms.DockStyle.Top;
            this.ManageGroups.FillColor = System.Drawing.Color.Transparent;
            this.ManageGroups.Font = new System.Drawing.Font("Sitka Small", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManageGroups.ForeColor = System.Drawing.Color.White;
            this.ManageGroups.Location = new System.Drawing.Point(0, 306);
            this.ManageGroups.Name = "ManageGroups";
            this.ManageGroups.Size = new System.Drawing.Size(219, 45);
            this.ManageGroups.TabIndex = 3;
            this.ManageGroups.Text = "Manage Groups";
            this.ManageGroups.Click += new System.EventHandler(this.ManageGroups_Click);
            // 
            // ManageProjectsBtn
            // 
            this.ManageProjectsBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(144)))), ((int)(((byte)(195)))));
            this.ManageProjectsBtn.BorderThickness = 1;
            this.ManageProjectsBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ManageProjectsBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ManageProjectsBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ManageProjectsBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ManageProjectsBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.ManageProjectsBtn.FillColor = System.Drawing.Color.Transparent;
            this.ManageProjectsBtn.Font = new System.Drawing.Font("Sitka Small", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManageProjectsBtn.ForeColor = System.Drawing.Color.White;
            this.ManageProjectsBtn.Location = new System.Drawing.Point(0, 261);
            this.ManageProjectsBtn.Name = "ManageProjectsBtn";
            this.ManageProjectsBtn.Size = new System.Drawing.Size(219, 45);
            this.ManageProjectsBtn.TabIndex = 2;
            this.ManageProjectsBtn.Text = "Manage Projects";
            this.ManageProjectsBtn.Click += new System.EventHandler(this.ManageProjectsBtn_Click);
            // 
            // ManageAdvisorsBtn
            // 
            this.ManageAdvisorsBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(144)))), ((int)(((byte)(195)))));
            this.ManageAdvisorsBtn.BorderThickness = 1;
            this.ManageAdvisorsBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ManageAdvisorsBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ManageAdvisorsBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ManageAdvisorsBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ManageAdvisorsBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.ManageAdvisorsBtn.FillColor = System.Drawing.Color.Transparent;
            this.ManageAdvisorsBtn.Font = new System.Drawing.Font("Sitka Small", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManageAdvisorsBtn.ForeColor = System.Drawing.Color.White;
            this.ManageAdvisorsBtn.Location = new System.Drawing.Point(0, 216);
            this.ManageAdvisorsBtn.Name = "ManageAdvisorsBtn";
            this.ManageAdvisorsBtn.Size = new System.Drawing.Size(219, 45);
            this.ManageAdvisorsBtn.TabIndex = 1;
            this.ManageAdvisorsBtn.Text = "Manage Advisors";
            this.ManageAdvisorsBtn.Click += new System.EventHandler(this.ManageAdvisorsBtn_Click);
            // 
            // ManageStudentsBtn
            // 
            this.ManageStudentsBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(144)))), ((int)(((byte)(195)))));
            this.ManageStudentsBtn.BorderThickness = 1;
            this.ManageStudentsBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ManageStudentsBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ManageStudentsBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ManageStudentsBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ManageStudentsBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.ManageStudentsBtn.FillColor = System.Drawing.Color.Transparent;
            this.ManageStudentsBtn.Font = new System.Drawing.Font("Sitka Small", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManageStudentsBtn.ForeColor = System.Drawing.Color.White;
            this.ManageStudentsBtn.Location = new System.Drawing.Point(0, 171);
            this.ManageStudentsBtn.Name = "ManageStudentsBtn";
            this.ManageStudentsBtn.Size = new System.Drawing.Size(219, 45);
            this.ManageStudentsBtn.TabIndex = 0;
            this.ManageStudentsBtn.Text = "Manage Students";
            this.ManageStudentsBtn.Click += new System.EventHandler(this.ManageStudentsBtn_Click);
            // 
            // LogoPanel
            // 
            this.LogoPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LogoPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.LogoPanel.Controls.Add(this.guna2PictureBox1);
            this.LogoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.LogoPanel.Location = new System.Drawing.Point(0, 0);
            this.LogoPanel.Margin = new System.Windows.Forms.Padding(0);
            this.LogoPanel.Name = "LogoPanel";
            this.LogoPanel.Size = new System.Drawing.Size(219, 171);
            this.LogoPanel.TabIndex = 0;
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.guna2PictureBox1.Image = global::DBMidproject.Properties.Resources.Your_paragraph_text__1_;
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox1.InitialImage")));
            this.guna2PictureBox1.Location = new System.Drawing.Point(21, 21);
            this.guna2PictureBox1.Margin = new System.Windows.Forms.Padding(15);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(177, 128);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox1.TabIndex = 0;
            this.guna2PictureBox1.TabStop = false;
            // 
            // HeaderPanel
            // 
            this.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(15)))), ((int)(((byte)(43)))));
            this.HeaderPanel.Controls.Add(this.ProjectNameLabel);
            this.HeaderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderPanel.Location = new System.Drawing.Point(219, 0);
            this.HeaderPanel.Margin = new System.Windows.Forms.Padding(0);
            this.HeaderPanel.Name = "HeaderPanel";
            this.HeaderPanel.Size = new System.Drawing.Size(724, 88);
            this.HeaderPanel.TabIndex = 1;
            // 
            // ProjectNameLabel
            // 
            this.ProjectNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ProjectNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.ProjectNameLabel.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProjectNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(144)))), ((int)(((byte)(195)))));
            this.ProjectNameLabel.Location = new System.Drawing.Point(244, 22);
            this.ProjectNameLabel.Name = "ProjectNameLabel";
            this.ProjectNameLabel.Size = new System.Drawing.Size(236, 44);
            this.ProjectNameLabel.TabIndex = 0;
            this.ProjectNameLabel.Text = "FYP Manager";
            this.ProjectNameLabel.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.ProjectNameLabel.Click += new System.EventHandler(this.ProjectNameLabel_Click);
            // 
            // MainScreenPanel
            // 
            this.MainScreenPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(177)))), ((int)(((byte)(177)))));
            this.MainScreenPanel.BackgroundImage = global::DBMidproject.Properties.Resources.Your_paragraph_text__1_;
            this.MainScreenPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MainScreenPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainScreenPanel.Location = new System.Drawing.Point(219, 88);
            this.MainScreenPanel.Margin = new System.Windows.Forms.Padding(0);
            this.MainScreenPanel.Name = "MainScreenPanel";
            this.MainScreenPanel.Size = new System.Drawing.Size(724, 409);
            this.MainScreenPanel.TabIndex = 2;
            this.MainScreenPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.MainScreenPanel_Paint);
            // 
            // ReportsBtn
            // 
            this.ReportsBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(146)))), ((int)(((byte)(144)))), ((int)(((byte)(195)))));
            this.ReportsBtn.BorderThickness = 1;
            this.ReportsBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ReportsBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ReportsBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ReportsBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ReportsBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.ReportsBtn.FillColor = System.Drawing.Color.Transparent;
            this.ReportsBtn.Font = new System.Drawing.Font("Sitka Small", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportsBtn.ForeColor = System.Drawing.Color.White;
            this.ReportsBtn.Location = new System.Drawing.Point(0, 441);
            this.ReportsBtn.Name = "ReportsBtn";
            this.ReportsBtn.Size = new System.Drawing.Size(219, 45);
            this.ReportsBtn.TabIndex = 6;
            this.ReportsBtn.Text = "Generate Reports";
            this.ReportsBtn.Click += new System.EventHandler(this.ReportsBtn_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 497);
            this.Controls.Add(this.MainScreenPanel);
            this.Controls.Add(this.HeaderPanel);
            this.Controls.Add(this.SideBarPanel);
            this.Name = "MainWindow";
            this.Text = "MainWindow";
            this.SideBarPanel.ResumeLayout(false);
            this.LogoPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.HeaderPanel.ResumeLayout(false);
            this.HeaderPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private Guna.UI2.WinForms.Guna2Panel SideBarPanel;
        private Guna.UI2.WinForms.Guna2Panel HeaderPanel;
        private Guna.UI2.WinForms.Guna2Panel MainScreenPanel;
        private Guna.UI2.WinForms.Guna2HtmlLabel ProjectNameLabel;
        private Guna.UI2.WinForms.Guna2Panel LogoPanel;
        private Guna.UI2.WinForms.Guna2Button ManageStudentsBtn;
        private Guna.UI2.WinForms.Guna2Button GroupProjectsBtn;
        private Guna.UI2.WinForms.Guna2Button ManageGroups;
        private Guna.UI2.WinForms.Guna2Button ManageProjectsBtn;
        private Guna.UI2.WinForms.Guna2Button ManageAdvisorsBtn;
        private Guna.UI2.WinForms.Guna2Button ManageEvaluationsBtn;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2Button ReportsBtn;
    }
}

