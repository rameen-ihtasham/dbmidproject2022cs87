﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMidproject.BL
{
    public class GroupStudent
    {
        public int GroupId { get; set; }
        public int StudentId {  get; set; }
        public int Status { get; set; }
        public DateTime AssignmentDate { get; set; }
        public GroupStudent(int groupId, int studentId)
        {
            GroupId = groupId;
            StudentId = studentId;
            Status = 3;
            AssignmentDate = DateTime.Today;
        }

    }
}
