﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMidproject.BL
{
    public class GroupProject
    {
        public int GroupId {  get; set; }
        public int ProjectTd {  get; set; }
        public DateTime AssignmentDate { get; set; }
        public GroupProject(int groupId, int projectTd)
        {
            GroupId = groupId;
            ProjectTd = projectTd;
            AssignmentDate = DateTime.Today;
        }
    }
}
