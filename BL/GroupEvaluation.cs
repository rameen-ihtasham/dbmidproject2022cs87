﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMidproject.BL
{
    public class GroupEvaluation
    {
        public int GroupId {  get; set; }
        public int EvaluationId { get; set; }
        public int ObtainedMarks {  get; set; }
        public DateTime EvaluationDate { get; set; }
        public GroupEvaluation(int groupId, int evaluationId, int obtainedMarks)
        {
            GroupId = groupId;
            EvaluationId = evaluationId;
            ObtainedMarks = obtainedMarks;
            EvaluationDate = DateTime.Today;
        }
    }
}
