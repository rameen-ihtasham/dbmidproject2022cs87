﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMidproject.BL
{
    public class Group
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }

        public Group(DateTime createdOn)
        {
            this.CreatedOn = createdOn;
        }
    }
}