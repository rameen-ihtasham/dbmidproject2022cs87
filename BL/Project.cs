﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMidproject.BL
{
    public class Project
    {
        public int Id {  get; set; }
        public string Title {  get; set; }
        public string Description {  get; set; }

        public Project(int id, string title, string description)
        {
            Id = id;
            Title = title;
            Description = description;
        }
    }
}
