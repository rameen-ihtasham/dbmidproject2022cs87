﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Management;

namespace DBMidproject.BL
{
    public class ProjectAdvisor
    {
        public int ProjectId { get; set; }
        public int AdvisorId { get; set; }
        public string AdvisorRole { get; set; }
        public DateTime AssignmentDate { get; set; }

        public ProjectAdvisor(int projectId, int advisorId, string advisorRole)
        {
            ProjectId = projectId;
            AdvisorId = advisorId;
            AdvisorRole = advisorRole;
            AssignmentDate = DateTime.Today;
        }

        public Dictionary<string, int> advisorRoles = new Dictionary<string, int>()
        {
            {"Main Advisor",11},
            {"Co-Advisor" , 12 },
            {"Industry Advisor" , 14 }
        };
        public int GetAdvisorRole(string role)
        {
            return advisorRoles[role];
        }
    }
}
