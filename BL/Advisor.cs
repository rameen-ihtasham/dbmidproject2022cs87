﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DBMidproject.BL
{
    public class Advisor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Designation { get; set; }
        public int Salary { get; set; }
        public Advisor(int id, string firstName, string lastName, string contact, string email, string gender, string dOB, int salary, string designation)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Contact = contact;
            Email = email;
            Gender = gender;
            DOB = dOB;
            Designation = designation;
            Salary = salary;
        }
        Dictionary<string, int> advisorDesignation = new Dictionary<string, int>()
        {
            {"Professor" , 6 },
            {"Associate Professor" ,7  },
            {"Assisstant Professor" ,8  },
            {"Lecturer" , 9 },
        };

        public int GetAdvisorDesignationId(string advisorDesignationValue)
        {
            return advisorDesignation[advisorDesignationValue];
        }




    }
}
