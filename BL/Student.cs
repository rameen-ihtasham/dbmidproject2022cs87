﻿using System;

namespace DBMidproject.BL
{
    public class Student
    {
        // Properties
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public Student(int id, string registrationNumber, string firstName, string lastName, string contact, string email, string gender, string dOB)
        {
            Id = id;
            RegistrationNumber = registrationNumber;
            FirstName = firstName;
            LastName = lastName;
            Contact = contact;
            Email = email;
            Gender = gender;
            DOB = dOB;
        }
    }
}
