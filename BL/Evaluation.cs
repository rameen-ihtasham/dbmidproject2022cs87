﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMidproject.BL
{
    public class Evaluation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalMarks { get; set; }
        public int TotalWeightage { get; set; }

        public Evaluation(int id ,string name, int totalMarks, int totalWeightage)
        {
            this.Id = id;
            Name = name;
            TotalMarks = totalMarks;
            TotalWeightage = totalWeightage;
        }
    }
}
