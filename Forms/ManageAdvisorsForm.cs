﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class ManageAdvisorsForm : Form
    {
        public ManageAdvisorsForm()
        {
            InitializeComponent();
            LoadGrid();
            
        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void guna2Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void SortByComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void SearchBar_TextChanged(object sender, EventArgs e)
        {

        }

        private void SearchByComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void guna2Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void DataTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {

        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = DataTable.SelectedRows[0];
            int id = (int)row.Cells[0].Value;
            string firstName = (string)row.Cells[1].Value;
            string secondName = (string)row.Cells[2].Value;
            string gender = (string)row.Cells[3].Value;
            string contact = (string)row.Cells[4].Value;
            string email = (string)row.Cells[5].Value;
            string dob = ((DateTime)row.Cells[6].Value).ToString();
            int salary = int.Parse(row.Cells[7].Value.ToString());
            string designation = (string)row.Cells[8].Value;
            Advisor advisor = new Advisor(id, firstName, secondName, contact, email, gender, dob,salary,designation);
            Action loadGrid = this.LoadGrid;
            Form form = new AddManagerForm(loadGrid, advisor);
            form.ShowDialog();

        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            Form form = new AddManagerForm(LoadGrid);
            form.ShowDialog();
        }

        public void LoadGrid()
        {
            DataTable.DataSource = Queries.GetAdvisorData();

        }

        private void DeleteBtn_Click_1(object sender, EventArgs e)
        {
            DataGridViewRow row = DataTable.SelectedRows[0];
            int id = (int)row.Cells[0].Value;
            string updatedName = "D-" + (string)row.Cells[1].Value;
            Queries.DeleteAdvisor(id, updatedName);
            LoadGrid();
        }
       
    }
}
