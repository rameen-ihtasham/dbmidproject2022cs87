﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class ManageEvaluationsForm : Form
    {
        public ManageEvaluationsForm()
        {
            InitializeComponent();
            LoadGrid();
        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            Action loadGrid = LoadGrid;
            Form form = new AddEvaluationForm(loadGrid);
            form.ShowDialog();
        }

        public void LoadGrid()
        {
            DataTable.DataSource = Queries.GetEvaluationData();

        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = DataTable.SelectedRows[0];
            int id = int.Parse(row.Cells[0].Value.ToString());
            string name = row.Cells[1].Value.ToString();
            int marks = int.Parse(row.Cells[2].Value.ToString());
            int weightage = int.Parse(row.Cells[3].Value.ToString());
            Evaluation evaluation = new Evaluation(id, name, marks, weightage);
            Action loadGrid = LoadGrid;
            Form form = new AddEvaluationForm(loadGrid,evaluation);
            form.ShowDialog();

        }
      
    }
}
