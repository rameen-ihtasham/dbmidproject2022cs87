﻿using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class AssignProjectsForm : Form
    {
        public AssignProjectsForm()
        {
            InitializeComponent();
            LoadGroupsData();
            LoadAssignedData();
        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void LoadGroupsData()
        {
            DataTable.DataSource = Queries.GetVoidGroups();
        }

        public void LoadAssignedData()
        {
            AssignedGroupsTable.DataSource = Queries.GetProjectsAssignedGroupsData();
        }
        private void AddBtn_Click(object sender, EventArgs e)
           
        {
            Action LoadRemainedGrid = LoadGroupsData;
            Action LoadAssignedGrid = LoadAssignedData;

            int groupId = (int)DataTable.SelectedRows[0].Cells[0].Value;
            Form form = new AddGroupProjectForm(groupId , LoadRemainedGrid,LoadAssignedGrid);
            form.ShowDialog();
        }
    }
}
