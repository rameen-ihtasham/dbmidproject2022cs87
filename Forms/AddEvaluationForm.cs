﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class AddEvaluationForm : Form
    {
        public Action LoadGrid;
        public Evaluation evaluation;
        public AddEvaluationForm(Action loadGrid , Evaluation evaluation = null)
        {
            this.LoadGrid = loadGrid;
            this.evaluation = evaluation;
            InitializeComponent();
            if(evaluation != null )
            {
                EvaluationNameTextbox.Text = evaluation.Name;
                TotalMarksTextbox.Text = evaluation.TotalMarks.ToString();
                TotalWeightageTextbox.Text = evaluation.TotalWeightage.ToString();
            }
        }
        public void GetEvaluation()
        {
            string name = EvaluationNameTextbox.Text;
            int marks= int.Parse(TotalMarksTextbox.Text.ToString());
            int weightage = int.Parse(TotalWeightageTextbox.Text.ToString());
            if (this.evaluation == null)
            {
                this.evaluation = new Evaluation(0, name, marks, weightage);
            }
            else
            {
                this.evaluation.Name = EvaluationNameTextbox.Text;
                this.evaluation.TotalMarks = int.Parse(TotalMarksTextbox.Text.ToString());
                this.evaluation.TotalWeightage = int.Parse(TotalWeightageTextbox.Text.ToString());
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            this.GetEvaluation();
            if(this.evaluation.Id == 0)
            {
                Queries.InsertEvaluation(this.evaluation);
            }
            else
            {
                Queries.UpdateEvaluation(this.evaluation);
            }
            this.LoadGrid();
            this.Close();
        }
    }
}
