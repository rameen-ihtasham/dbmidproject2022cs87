﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class ManageProjectsForm : Form
    {
        public ManageProjectsForm()
        {
            InitializeComponent();
            LoadGrid();
        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            Action loadGrid = LoadGrid;
            Form form = new AddProjectForm(loadGrid);   
            form.ShowDialog();
        }

        public void LoadGrid()
        {
            DataTable.DataSource = Queries.GetProjectsData();
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = DataTable.SelectedRows[0];
            int id = (int)row.Cells[0].Value;
            string description = row.Cells[2].Value.ToString();
            string title = row.Cells[1].Value.ToString();
            Project project = new Project(id, description, title);
            Action loadGrid = LoadGrid;
            Form form = new AddProjectForm(loadGrid,project);
            form.ShowDialog();
        }

        private void AddAdvisorBtn_Click(object sender, EventArgs e)
        {
            int id = (int)DataTable.SelectedRows[0].Cells[0].Value;
            Form form = new AddGroupAdvisorFormcs(id);
            form.ShowDialog();
        }
       
    }
}
