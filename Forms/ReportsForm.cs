﻿using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class ReportsForm : Form
    {
        public ReportsForm()
        {
            InitializeComponent();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            DataTable dt = ReportQueries.ReportOneQuery();
            ExportToPdf.GeneratePDF(dt, "D:\\UET\\DB\\PDFs\\report1.pdf", "List of Students & Advisory Boards of Projects");
            MessageBox.Show("Pdf Generated Successfully");
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            DataTable dt = ReportQueries.ReportTwoQuery();
            ExportToPdf.GeneratePDF(dt, "D:\\UET\\DB\\PDFs\\report2.pdf", "Students Marks in Projects Evaluation");
            MessageBox.Show("Pdf Generated Successfully");
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            DataTable dt = ReportQueries.ReportThreeQuery();
            ExportToPdf.GeneratePDF(dt, "D:\\UET\\DB\\PDFs\\report3.pdf", "Group Evaluation Marks");
            MessageBox.Show("Pdf Generated Successfully");
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
            DataTable dt = ReportQueries.ReportFourQuery();
            ExportToPdf.GeneratePDF(dt, "D:\\UET\\DB\\PDFs\\report4.pdf", "Top Three Project Groups");
            MessageBox.Show("Pdf Generated Successfully");
        }

        private void guna2Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            DataTable dt = ReportQueries.ReportFiveQuery();
            ExportToPdf.GeneratePDF(dt, "D:\\UET\\DB\\PDFs\\report5.pdf", "Gender Based Performance Report");
            MessageBox.Show("Pdf Generated Successfully");
        }
    }
}
