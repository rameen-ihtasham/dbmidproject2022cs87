﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class MarkEvaluationForm : Form
    {
        public int GroupID;
        public Dictionary<int,string> ComboBoxData; 
        public MarkEvaluationForm(int groupId)
        {
            this.GroupID = groupId;
            this.ComboBoxData = Queries.GetComboBoxData(this.GroupID);
            InitializeComponent();
            LoadDataInComboBox();                  
            LoadTotalMarks();
            if (ComboBoxData.Count ==  0)
            {
                AddBtn.Visible = false;
            }
            
            
        }

        public void LoadDataInComboBox()
        {

            if (ComboBoxData.Count == 0)
            {
                EvaluationNameCombobox.Text = "";
            }
            else
            {
                EvaluationNameCombobox.DataSource = new BindingSource(ComboBoxData.Values, null);
            }
            
        }
        public void LoadTotalMarks()
        {
            if (ComboBoxData.Count > 0)
            {
                string valueToFind = EvaluationNameCombobox.SelectedValue.ToString();
                int eID = ComboBoxData.FirstOrDefault(x => x.Value == valueToFind).Key;
                TotalMarksTextbox.Text = Queries.GetEvaluationTotalMarks(eID).ToString();
            }
            else
            {
                TotalMarksTextbox.Text = "";
            }
        }

        private void EvaluationNameCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTotalMarks();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            int obtainedMarks = int.Parse(ObtainedMarksTextbox.Text);
            if(obtainedMarks > int.Parse(TotalMarksTextbox.Text ))
            {
                MessageBox.Show("Obtained marks should be less than " + TotalMarksTextbox.Text);
            }
            else if(obtainedMarks < 0)
            {
                MessageBox.Show("Entered obtained marks are negative.");

            }
            else
            {
                string valueToFind = EvaluationNameCombobox.SelectedValue.ToString();
                int eID = ComboBoxData.FirstOrDefault(x => x.Value == valueToFind).Key;
                GroupEvaluation groupEvaluation = new GroupEvaluation(this.GroupID, eID, obtainedMarks);
                Queries.InsertGroupEvaluation(groupEvaluation);
                this.Close();
            }
          
        }
    }
}
