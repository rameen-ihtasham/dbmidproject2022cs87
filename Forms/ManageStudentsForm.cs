﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class ManageStudentsForm : Form
    {
        public ManageStudentsForm()
        {
            InitializeComponent();
            LoadGrid();
           
        }


        private void SearchBar_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddStudentBtn_Click(object sender, EventArgs e)
        {
            Action loadGrid = LoadGrid;
            Form form = new AddStudentFrom(loadGrid);
            form.ShowDialog();
        }

        private void DataTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void LoadGrid()
        {
            DataTable.DataSource = Queries.GetStudentsData();
        }

        private void Deletebtn_Click(object sender, EventArgs e)
        {
          //  int id = (int)DataTable.SelectedRows[0].Cells[0].Value;
           // Queries.DeleteStudent(id);
           // LoadGrid();
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = DataTable.SelectedRows[0];
            int id = (int)row.Cells[0].Value;
            string regNo = (string)row.Cells[1].Value;
            string firstName = (string)row.Cells[2].Value;
            string secondName = (string)row.Cells[3].Value;
            string gender = (string)row.Cells[4].Value;
            string contact = (string)row.Cells[5].Value;
            string email = (string)row.Cells[6].Value;
            string dob = ((DateTime)row.Cells[7].Value).ToString();
            Student student = new Student(id, regNo, firstName, secondName, contact, email, gender, dob);

            Action loadGrid = LoadGrid;
            Form form = new AddStudentFrom(loadGrid, student);
            form.ShowDialog();
        }

        private void guna2Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void DeleteBtn_Click_1(object sender, EventArgs e)
        {
            int id = (int)DataTable.SelectedRows[0].Cells[0].Value;
            string name = "D-" + DataTable.SelectedRows[0].Cells[2].Value.ToString() ;
            Queries.DeleteStudent(id,name);
            LoadGrid();
        }
       
       
    }
}
