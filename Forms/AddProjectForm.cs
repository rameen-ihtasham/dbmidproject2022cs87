﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class AddProjectForm : Form
    {
        public Action LoadGrid;
        public Project project;
        public AddProjectForm(Action loadGrid , Project project = null)
        {
            this.project = project;
            this.LoadGrid = loadGrid;
            InitializeComponent();
            if(!(project is null))
            {
                ProjectTitleTextbox.Text = project.Title;
                ProjectDiscriptionTextbox.Text = project.Description;
            }
        }
        public void GetProject()
        {
            string projectTitle = ProjectTitleTextbox.Text;
            string projectDescription = ProjectDiscriptionTextbox.Text;

            if(project is null)
            {
                this.project = new Project(0,projectTitle,projectDescription);
            }
            else
            {
                this.project.Title = projectTitle;
                this.project.Description = projectDescription;

            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            this.GetProject();
            if(this.project.Id == 0)
            {
                Queries.InsertProject(this.project);

            }
            else{
                Queries.UpdateProject(this.project);
            }
            this.LoadGrid();
            this.Close();
        }
    }
}
