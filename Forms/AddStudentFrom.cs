﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class AddStudentFrom : Form
    {
        public Student student;
        public Action loadGrid;
        public AddStudentFrom(Action loadGrid, Student student = null)
        {
            this.student = student;
            this.loadGrid = loadGrid;
            InitializeComponent();
            if (!(this.student is null))
            {
                FirstNameTextbox.Text = student.FirstName;
                LastNameTextbox.Text = student.LastName;
                RegistrationNumberTextbox.Text = student.RegistrationNumber;
                ContactTextbox.Text = student.Contact;
                EmailTextbox.Text = student.Email;
                GenderCombobox.Text = student.Gender;
                DOBpicker.Text = student.DOB;
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            this.GetStudent();
            if (this.student.Id == 0)
            {
                Queries.InsertStudent(this.student);
            }
            else
            {
                Queries.UpdateStudent(this.student);
            }
            this.loadGrid();
            this.Close();
        }

        private void GetStudent()
        {
            string firstName = FirstNameTextbox.Text;
            string lastName = LastNameTextbox.Text;
            string registrationNumber = RegistrationNumberTextbox.Text;
            string contact = ContactTextbox.Text;
            string email = EmailTextbox.Text;
            string gender = GenderCombobox.Text;
            string dob = DOBpicker.Text;
            if (this.student == null)
            {
                this.student = new Student(0, registrationNumber, firstName, lastName, contact, email, gender, dob);
            }
            else
            {
                this.student.FirstName = firstName;
                this.student.LastName = lastName;
                this.student.RegistrationNumber = registrationNumber;
                this.student.Contact = contact;
                this.student.Email = email;
                this.student.Gender = gender;
                this.student.DOB = dob;
            }
        }

        private void FirstNameTextbox_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
