﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class AddGroupProjectForm : Form
    {
        public int GroupId;
        public Action RemainedGrid;
        public Action AssignedGrid;

        public AddGroupProjectForm(int groupId , Action RGrid,Action AGrid)
        {
            InitializeComponent();
            LoadProjectData();
            GroupId = groupId;
            this.RemainedGrid = RGrid;
            this.AssignedGrid = AGrid;
        }

        public void LoadProjectData()
        {
            DataTable.DataSource = Queries.GetAvailableGroupProjects();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            int proId = (int)DataTable.SelectedRows[0].Cells[0].Value;
            GroupProject groupProject = new GroupProject(this.GroupId, proId);
            Queries.AssignGroupProject(groupProject);
            this.RemainedGrid();
            this.AssignedGrid();
            this.Close();
            
        }
    }
}
