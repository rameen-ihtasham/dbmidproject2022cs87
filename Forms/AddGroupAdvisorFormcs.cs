﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class AddGroupAdvisorFormcs : Form
    {
        public int ProjectId;
        public AddGroupAdvisorFormcs( int ProId )
        {
            this.ProjectId  = ProId;
            InitializeComponent();
            LoadProjectAdvisorsGrid();
            LoadAvailableAdvisorsGrid();
        }

        public void LoadProjectAdvisorsGrid()
        {
            ProjectAdvisorsTable.DataSource = Queries.GetProjectAdvisorData(this.ProjectId);
        }
        public void LoadAvailableAdvisorsGrid()
        {
            AvailableAdvisorsTable.DataSource = Queries.GetAvailableAdvisorsData(this.ProjectId);
        }

        private void RemoveBtn_Click(object sender, EventArgs e)
        {
            int AdvisorId = (int)ProjectAdvisorsTable.SelectedRows[0].Cells[0].Value;
            Queries.RemoveAdvisorFromProject(this.ProjectId, AdvisorId);
            LoadProjectAdvisorsGrid();
            LoadAvailableAdvisorsGrid();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if(PositionCombobox.Text != "")  
            {
                int AdvisorId = (int)AvailableAdvisorsTable.SelectedRows[0].Cells[0].Value;
                ProjectAdvisor projectAdvisor = new ProjectAdvisor(this.ProjectId,AdvisorId,PositionCombobox.Text);
                Queries.AddAdvisorToProject(projectAdvisor);
                LoadProjectAdvisorsGrid();
                LoadAvailableAdvisorsGrid();
            }
            else
            {
                MessageBox.Show("First Select The Role");
            }
        }
    }
}
