﻿using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class ManageGroupsForm : Form
    {
        public ManageGroupsForm()
        {
            InitializeComponent();
            LoadGrid();
        }

        public void LoadGrid()
        {
            DataTable.DataSource = Queries.GetGroupData();
        }

        private void AddGroupBtn_Click(object sender, EventArgs e)
        {
            Queries.InsertGroup();
            LoadGrid();
        }

        private void AddStudentBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = DataTable.SelectedRows[0];
            int id = (int)row.Cells[0].Value;
            Form form = new AddGroupStudentsForm(id);
            form.ShowDialog();
        }

        private void MarkEvaluationBtn_Click(object sender, EventArgs e)
        {
            int GroupId = (int)DataTable.SelectedRows[0].Cells[0].Value;
            Form form = new MarkEvaluationForm(GroupId);
            form.ShowDialog();
        }
       
    }
}
