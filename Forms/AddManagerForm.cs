﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class AddManagerForm : Form
    {
        public Advisor advisor;
        public Action LoadGrid;
        public AddManagerForm(Action loadGrid,Advisor advisor = null)
        {
            this.LoadGrid = loadGrid;
            this.advisor = advisor;
            InitializeComponent();
            //DesignationCombobox.
            if(!(this.advisor is null))
            {
                FirstNameTextbox.Text = advisor.FirstName;
                LastNameTextbox.Text = advisor.LastName;
                ContactTextbox.Text = advisor.Contact;
                DesignationCombobox.Text = advisor.Designation;
                EmailTextbox.Text = advisor.Email;
                DOBpicker.Text = advisor.DOB;
                SalaryTextbox.Text = advisor.Salary.ToString();
                GenderCombobox.Text = advisor.Gender;
            }
           
        }

        private void guna2Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public void GetAdvisor()
        {
            string firstName = FirstNameTextbox.Text;
            string lastName = LastNameTextbox.Text;
            string contact = ContactTextbox.Text;
            string email = EmailTextbox.Text;
            string gender = GenderCombobox.Text;
            string dob = DOBpicker.Text;
            string designation = DesignationCombobox.Text;
            int salary = int.Parse(SalaryTextbox.Text);

            if(this.advisor is null)
            {
                this.advisor = new Advisor(0, firstName, lastName, contact, email, gender, dob, salary, designation);
            }
            else
            {
                this.advisor.FirstName = firstName;
                this.advisor.LastName = lastName;
                this.advisor.Contact = contact;
                this.advisor.Email = email;
                this.advisor.Gender = gender;
                this.advisor.DOB = dob;
                this.advisor.Salary = salary;
                this.advisor.Designation = designation;
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            this.GetAdvisor();
            if (this.advisor.Id == 0)
            {
                Queries.InsertAdvisor(this.advisor);
            }
            else
            {
                Queries.UpdateAdvisor(this.advisor);
            }
            this.LoadGrid();
            this.Close();
        }
    }
}
