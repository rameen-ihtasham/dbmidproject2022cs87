﻿using DBMidproject.BL;
using DBMidproject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject.Forms
{
    public partial class AddGroupStudentsForm : Form
    {
        public int GroupID;
        public AddGroupStudentsForm(int id)
        {
            this.GroupID = id;  
            InitializeComponent();
            LoadGroupStudentGrid();
            LoadAvailableStudentsGrid();
        }
        public void LoadGroupStudentGrid()
        {
           GroupStudentsTable.DataSource =  Queries.GetGroupStudentData(this.GroupID);
           
        }
        public void LoadAvailableStudentsGrid()
        {
            AvailableStudentsTable.DataSource = Queries.GetAvailableStudentsData();
        }

        private void RemoveBtn_Click(object sender, EventArgs e)
        {
            int id = (int)GroupStudentsTable.SelectedRows[0].Cells[1].Value;
            Queries.RemoveStudentFromGroup(id);
            LoadGroupStudentGrid();
            LoadAvailableStudentsGrid();
        }

        private void StatusCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = (int)GroupStudentsTable.SelectedRows[0].Cells[1].Value;
            string status = StatusCombobox.Text;
            Queries.ChangeGroupStudentStatus(id, status);
            LoadGroupStudentGrid();

        }

        private void GroupStudentsTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (GroupStudentsTable.RowCount <= 3)
            {
                int StuId = (int)AvailableStudentsTable.SelectedRows[0].Cells[0].Value;
                GroupStudent groupStudent = new GroupStudent(this.GroupID, StuId);
                Queries.AddStudentToGroup(groupStudent);
                LoadGroupStudentGrid();
                LoadAvailableStudentsGrid();
            }
            else
            {
                MessageBox.Show("No more space in this Group!!");
            }
        }
    }
}
