﻿using DBMidproject.DataAccess;
using DBMidproject.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidproject
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            AddChildForm(new DisplayForm());
            
        }

        private void ProjectNameLabel_Click(object sender, EventArgs e)
        {

        }

        private void MainScreenPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private Form activeForm = null;
        private void AddChildForm(Form childForm)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            MainScreenPanel.Controls.Add(childForm);
            childForm.BringToFront();
            MainScreenPanel.Tag = childForm;
            childForm.Show();
        }

        private void ManageStudentsBtn_Click(object sender, EventArgs e)
        {
            AddChildForm(new ManageStudentsForm());
        }

        private void ManageAdvisorsBtn_Click(object sender, EventArgs e)
        {
            AddChildForm(new ManageAdvisorsForm());
        }

        private void ManageProjectsBtn_Click(object sender, EventArgs e)
        {
            AddChildForm(new ManageProjectsForm());
        }

        private void GroupProjectsBtn_Click(object sender, EventArgs e)
        {
            AddChildForm(new AssignProjectsForm());
        }

        private void ManageEvaluationsBtn_Click(object sender, EventArgs e)
        {
            AddChildForm(new ManageEvaluationsForm());
        }

        private void ManageGroups_Click(object sender, EventArgs e)
        {
            AddChildForm(new ManageGroupsForm());
        }

        private void ReportsBtn_Click(object sender, EventArgs e)
        {
            AddChildForm(new ReportsForm());
        }
    }
}
