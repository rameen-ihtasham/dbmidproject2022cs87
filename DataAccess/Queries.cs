﻿using DBMidproject.BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Management;
using System.Windows.Forms;

namespace DBMidproject.DataAccess
{

    static public class Queries
    {
        public static SqlConnection con = Configuration.getInstance().getConnection();
        static public DataTable GetStudentsData()
        {
            SqlCommand cmd = new SqlCommand("SELECT Person.Id, Student.RegistrationNo, Person.FirstName, Person.LastName, Lookup.Value AS Gender, Person.Contact, Person.Email, Person.DateOfBirth FROM Person JOIN Student ON Person.Id = Student.Id JOIN Lookup ON Person.Gender = Lookup.Id AND Left(Person.FirstName,2) <> 'D-' ;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        static public void InsertStudent(Student student)
        {
            SqlCommand cmd = new SqlCommand("Insert into Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) OUTPUT Inserted.ID values (@first, @second, @contact, @email, @dob, @gender);", con);
            cmd.Parameters.AddWithValue("@first", student.FirstName);
            cmd.Parameters.AddWithValue("@second", student.LastName);
            cmd.Parameters.AddWithValue("@contact", student.Contact);
            cmd.Parameters.AddWithValue("@email", student.Email);
            cmd.Parameters.AddWithValue("@dob", DateTime.Parse(student.DOB));
            cmd.Parameters.AddWithValue("@gender", student.Gender == "Male" ? 1 : 2);
            int personId = (int)cmd.ExecuteScalar();

            cmd = new SqlCommand("Insert Into Student Values (@id, @regNo)", con);
            cmd.Parameters.AddWithValue("@id", personId);
            cmd.Parameters.AddWithValue("@regNo", student.RegistrationNumber);
            cmd.ExecuteNonQuery();
        }

        static public void UpdateStudent(Student student)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName=@first , LastName=@second , Contact=@contact , Email=@email , DateOfBirth=@dob , Gender=@gender WHERE Id=@id", con);
            cmd.Parameters.AddWithValue("@id", student.Id);
            cmd.Parameters.AddWithValue("@first", student.FirstName);
            cmd.Parameters.AddWithValue("@second", student.LastName);
            cmd.Parameters.AddWithValue("@contact", student.Contact);
            cmd.Parameters.AddWithValue("@email", student.Email);
            cmd.Parameters.AddWithValue("@dob", DateTime.Parse(student.DOB));
            cmd.Parameters.AddWithValue("@gender", student.Gender == "Male" ? 1 : 2);
            cmd.ExecuteNonQuery();

            cmd = new SqlCommand("UPDATE Student SET RegistrationNo=@regNo WHERE Id=@id", con);
            cmd.Parameters.AddWithValue("@id", student.Id);
            cmd.Parameters.AddWithValue("@regNo", student.RegistrationNumber);
            cmd.ExecuteNonQuery();
        }

        static public void DeleteStudent(int id , string updatedName)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName = @name WHERE id = @id;", con);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@name" ,updatedName);
            cmd.ExecuteNonQuery();
            SqlCommand ccmd = new SqlCommand("DELETE FROM GroupStudent WHERE StudentId =@id ;", con);
            ccmd.Parameters.AddWithValue("@id", id);
            ccmd.ExecuteNonQuery();
        }

        public static void DeleteAdvisor(int id , string updatedName)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName = @name WHERE id = @id;", con);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@name", updatedName);
            cmd.ExecuteNonQuery();
            SqlCommand ccmd = new SqlCommand("DELETE FROM ProjectAdvisor WHERE ProjectAdvisor.AdvisorId = @id ;", con);
            ccmd.Parameters.AddWithValue("@id", id);
            ccmd.ExecuteNonQuery();
        }

        public static DataTable GetAdvisorData()
        {
            SqlCommand cmd = new SqlCommand("SELECT Person.Id, Person.FirstName, Person.LastName, Lookup.Value AS Gender, Person.Contact, Person.Email, Person.DateOfBirth,Advisor.Salary,(SELECT Value FROM Lookup WHERE Id = Advisor.Designation) AS Designation FROM Person JOIN Advisor ON Person.Id = Advisor.Id JOIN Lookup ON Person.Gender = Lookup.Id AND Left(Person.FirstName,2) <> 'D-';", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public static void InsertAdvisor(Advisor advisor)
        {            
            SqlCommand cmd = new SqlCommand("Insert into Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) OUTPUT Inserted.ID values (@first, @second, @contact, @email, @dob, @gender);", con);
            cmd.Parameters.AddWithValue("@first", advisor.FirstName);
            cmd.Parameters.AddWithValue("@second", advisor.LastName);
            cmd.Parameters.AddWithValue("@contact", advisor.Contact);
            cmd.Parameters.AddWithValue("@email", advisor.Email);
            cmd.Parameters.AddWithValue("@dob", DateTime.Parse(advisor.DOB));
            cmd.Parameters.AddWithValue("@gender", advisor.Gender == "Male" ? 1 : 2);
            int personId = (int)cmd.ExecuteScalar();

            cmd = new SqlCommand("Insert Into Advisor Values (@id, @designation , @salary)", con);
            cmd.Parameters.AddWithValue("@id", personId);
            cmd.Parameters.AddWithValue("@designation", advisor.GetAdvisorDesignationId(advisor.Designation));
            cmd.Parameters.AddWithValue("@salary", advisor.Salary);
            cmd.ExecuteNonQuery();            
        }
        public static void UpdateAdvisor(Advisor advisor)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName=@first , LastName=@second , Contact=@contact , Email=@email , DateOfBirth=@dob , Gender=@gender WHERE Id=@id", con);
            cmd.Parameters.AddWithValue("@id", advisor.Id);
            cmd.Parameters.AddWithValue("@first", advisor.FirstName);
            cmd.Parameters.AddWithValue("@second", advisor.LastName);
            cmd.Parameters.AddWithValue("@contact", advisor.Contact);
            cmd.Parameters.AddWithValue("@email", advisor.Email);
            cmd.Parameters.AddWithValue("@dob", DateTime.Parse(advisor.DOB));
            cmd.Parameters.AddWithValue("@gender", advisor.Gender == "Male" ? 1 : 2);
            cmd.ExecuteNonQuery();

            cmd = new SqlCommand("UPDATE Advisor SET Designation = @designation , Salary = @salary WHERE Id = @id", con);
            cmd.Parameters.AddWithValue("@designation", advisor.GetAdvisorDesignationId(advisor.Designation));
            cmd.Parameters.AddWithValue("@id", advisor.Id);
            cmd.Parameters.AddWithValue("@salary", advisor.Salary);
            cmd.ExecuteNonQuery();

        }

        public static DataTable GetProjectsData()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project",con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void InsertProject(Project p)
        {
            SqlCommand cmd = new SqlCommand("INSERT into Project (Description,Title) VALUES (@description,@title)",con);
            cmd.Parameters.AddWithValue("@description", p.Description);
            cmd.Parameters.AddWithValue("@title", p.Title);
            cmd.ExecuteNonQuery();
        }
        public static void UpdateProject(Project p)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Project SET Description =@description, Title= @title WHERE Id = @id" , con);
            cmd.Parameters.AddWithValue("@description", p.Description);
            cmd.Parameters.AddWithValue("@title", p.Title);
            cmd.Parameters.AddWithValue("@id", p.Id);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetEvaluationData()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Evaluation", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void InsertEvaluation(Evaluation e)
        {
            SqlCommand cmd = new SqlCommand("INSERT into Evaluation (Name,TotalMarks,TotalWeightage) VALUES (@name,@totalMarks,@totalWeightage)",con);
            cmd.Parameters.AddWithValue("@name", e.Name);
            cmd.Parameters.AddWithValue("@totalMarks", e.TotalMarks);
            cmd.Parameters.AddWithValue("@totalWeightage", e.TotalWeightage);
            cmd.ExecuteNonQuery();

        }
        public static void UpdateEvaluation(Evaluation e) 
        {
            SqlCommand cmd = new SqlCommand("UPDATE Evaluation SET Name = @name , TotalMarks = @totalMarks , TotalWeightage = @totalWeightage WHERE Id = @id",con);
            cmd.Parameters.AddWithValue("@name", e.Name);
            cmd.Parameters.AddWithValue("@totalMarks", e.TotalMarks);
            cmd.Parameters.AddWithValue("@totalWeightage", e.TotalWeightage);
            cmd.Parameters.AddWithValue("@id" , e.Id);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetGroupData()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM [Group]",con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void InsertGroup()
        {            
            SqlCommand cmd = new SqlCommand("Insert into [Group] (Created_On) VALUES (@date)", con);
            cmd.Parameters.AddWithValue("@date", DateTime.Today);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetGroupStudentData(int id)
        {
            SqlCommand cmd = new SqlCommand("Select [Group].Id,GroupStudent.StudentId,Person.FirstName,Person.LastName,GroupStudent.AssignmentDate,Lookup.Value from [Group] JOIN GroupStudent on ([Group].Id  = GroupStudent.GroupId AND [Group].Id = @id) Join Person on Person.Id = GroupStudent.StudentId join Lookup on GroupStudent.Status = Lookup.Id;", con);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static DataTable GetAvailableStudentsData()
        {
            SqlCommand cmd = new SqlCommand("Select Student.Id, Student.RegistrationNo,Person.FirstName,Person.LastName,Person.Contact,Person.Email,Person.DateOfBirth,Lookup.Value AS Gender From Student JOIN Person ON Student.Id = Person.Id JOIN Lookup on Person.Gender = Lookup.Id AND LEFT(Person.FirstName,2) <> 'D-' WHERE Student.Id NOT IN (Select StudentId from GroupStudent);", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void RemoveStudentFromGroup(int id)
        {
            SqlCommand cmd = new SqlCommand("Delete from GroupStudent where StudentId = @id; ", con);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.ExecuteNonQuery();
        }

        public static void ChangeGroupStudentStatus(int id,string status)
        {
            SqlCommand cmd = new SqlCommand("Update GroupStudent  SET Status = (select Lookup.Id from Lookup where Lookup.value = @newstatus ) where GroupStudent.StudentId = @id;", con);
            cmd.Parameters.AddWithValue("@newstatus", status);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.ExecuteNonQuery();
        }

        public static void AddStudentToGroup(GroupStudent gs)
        {
            SqlCommand cmd = new SqlCommand("Insert into GroupStudent (GroupId , StudentId,Status,AssignmentDate) VALUES (@groupid,@studentid,@status,@assignmentdate);", con);
            cmd.Parameters.AddWithValue("@groupid", gs.GroupId);
            cmd.Parameters.AddWithValue("@studentid", gs.StudentId);
            cmd.Parameters.AddWithValue("@status", gs.Status);
            cmd.Parameters.AddWithValue("@assignmentdate", gs.AssignmentDate);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetProjectAdvisorData(int ProId)
        {
            SqlCommand cmd = new SqlCommand("Select ProjectAdvisor.AdvisorId,ProjectAdvisor.ProjectId,ProjectAdvisor.AssignmentDate,Person.FirstName,Person.LastName,Person.Contact,Person.Email,Person.DateOfBirth,Lookup.Value as [Role],l.Value as [Gender] from ProjectAdvisor JOIN Person on ProjectAdvisor.AdvisorId = Person.Id join Lookup on ProjectAdvisor.AdvisorRole = Lookup.Id join Lookup as l on Person.Gender = l.Id Where ProjectAdvisor.ProjectId = @id ;", con);
            cmd.Parameters.AddWithValue("@id", ProId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public static DataTable GetAvailableAdvisorsData(int ProId)
        {
            SqlCommand cmd = new SqlCommand("select Advisor.Id , Advisor.Salary,Person.FirstName,Person.LastName,Person.Contact,Person.Email,Person.DateOfBirth,Lookup.Value AS Gender from Advisor JOIN Person on Advisor.Id = Person.Id JOIN Lookup on Person.Gender = Lookup.id AND LEFT(Person.FirstName,2) <> 'D-' where Advisor.Id NOT IN(select ProjectAdvisor.AdvisorId from ProjectAdvisor where ProjectAdvisor.ProjectId = @id);", con);
            cmd.Parameters.AddWithValue("@id", ProId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void RemoveAdvisorFromProject(int ProId , int AdvId)
        {
            SqlCommand cmd = new SqlCommand("DELETE FROM ProjectAdvisor where ProjectId = @pid AND AdvisorId = @aid;", con);
            cmd.Parameters.AddWithValue("@pid", ProId);
            cmd.Parameters.AddWithValue("@aid", AdvId);
            cmd.ExecuteNonQuery();
        }
        public static void AddAdvisorToProject(ProjectAdvisor pa)
        {
            SqlCommand cmd = new SqlCommand("INSERT into ProjectAdvisor (AdvisorId,ProjectId,AdvisorRole,AssignmentDate) VALUES (@aid,@pid,@role,@date);", con);
            cmd.Parameters.AddWithValue("@aid", pa.AdvisorId);
            cmd.Parameters.AddWithValue("@pid", pa.ProjectId);
            cmd.Parameters.AddWithValue("@role", pa.GetAdvisorRole(pa.AdvisorRole));
            cmd.Parameters.AddWithValue("@date", pa.AssignmentDate);
            cmd.ExecuteNonQuery();

        }

        public static DataTable GetAvailableGroupProjects()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project Where Project.Id NOT IN (SELECT ProjectId FROM GroupProject);", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static DataTable GetVoidGroups()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM [Group] WHERE [Group].Id NOT IN (SELECT GroupId FROM GroupProject);", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public static DataTable GetProjectsAssignedGroupsData()
        {
            SqlCommand cmd = new SqlCommand("SELECT GroupProject.ProjectId,Project.Title,Project.Description,[Group].Id AS GroupId,GroupProject.AssignmentDate FROM GroupProject JOIN [Group] ON [Group].Id = GroupProject.GroupId JOIN Project ON Project.Id = GroupProject.ProjectId ;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void AssignGroupProject(GroupProject gp)
        {
            SqlCommand cmd = new SqlCommand("Insert into GroupProject (ProjectId,GroupId,AssignmentDate) Values(@pid,@gid,@date); ", con);
            cmd.Parameters.AddWithValue("@pid", gp.ProjectTd);
            cmd.Parameters.AddWithValue("@gid", gp.GroupId);
            cmd.Parameters.AddWithValue("@date", gp.AssignmentDate);
            cmd.ExecuteNonQuery();
        }

        public static Dictionary<int,string> GetComboBoxData(int groupId)
        {
            Dictionary< int,string> comboboxdata = new Dictionary<int,string>();
            SqlCommand cmd = new SqlCommand("SELECT DISTINCT Evaluation.Id,Evaluation.Name FROM Evaluation LEFT JOIN GroupEvaluation on Evaluation.Id = GroupEvaluation.EvaluationId WHERE Evaluation.Id NOT IN (SELECT EvaluationId From GroupEvaluation WHERE GroupEvaluation.GroupId = @id );", con);
            cmd.Parameters.AddWithValue("@id", groupId);
            SqlDataReader reader = cmd.ExecuteReader();
            while(reader.Read())
            {
                int id = reader.GetInt32(0);
                string name = reader.GetString(1);
                comboboxdata.Add(id, name);
            }
            reader.Close();
            return comboboxdata;
        }

        public static int GetEvaluationTotalMarks(int EvaId)
        {
            
            SqlCommand cmd = new SqlCommand("SELECT TotalMarks FROM Evaluation WHERE Evaluation.Id = @id;", con);
            cmd.Parameters.AddWithValue("@id", EvaId);
            object result = cmd.ExecuteScalar();
            if (result == null)
            {
                return 0;
            }
            else
            {
                int marks = (int)cmd.ExecuteScalar();
                return marks;
            }
            
        }

        public static void InsertGroupEvaluation(GroupEvaluation groupEvaluation)
        {
            SqlCommand cmd = new SqlCommand("INSERT into GroupEvaluation (GroupId,EvaluationId,ObtainedMarks,EvaluationDate) VALUES (@gid,@eid,@marks,@date);", con);
            cmd.Parameters.AddWithValue("@gid", groupEvaluation.GroupId);
            cmd.Parameters.AddWithValue("@eid", groupEvaluation.EvaluationId);
            cmd.Parameters.AddWithValue("@marks", groupEvaluation.ObtainedMarks);
            cmd.Parameters.AddWithValue("@date", groupEvaluation.EvaluationDate);
            cmd.ExecuteNonQuery();
        }

        public static List<string> GetColumnsForComboBox(string tableName)
        {
            List<string> columns = new List<string>();
            SqlCommand cmd  = new SqlCommand("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @name;", con);
            cmd.Parameters.AddWithValue("@name", tableName);
            SqlDataReader reader = cmd.ExecuteReader();
            while(reader.Read())
            {
                string name = reader.GetString(0);
                columns.Add(name);
                
            }
            reader.Close();
            return columns;

        }
     


    }
}
