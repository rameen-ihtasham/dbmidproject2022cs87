﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace DBMidproject.DataAccess
{
    public static class ReportQueries
    {
        public static SqlConnection con = Configuration.getInstance().getConnection();

        public static DataTable ReportOneQuery()
        {
            string query = "select  PJ.Id AS ProjectId , PJ.Title AS ProjectTitle,PJ.Description AS ProjectDescription , PA.AdvisorId AS AdvisorId,P.FirstName+' ' +P.LastName AS AdvisorName , GS.StudentId AS StudentId , PR.FirstName + ' ' + PR.LastName AS SturdentName from  Project PJ JOIN  ProjectAdvisor PA on PJ.Id = PA.ProjectId JOIN Person P on PA.AdvisorId = P.Id JOIN GroupProject GP on GP.ProjectId = PJ.Id JOIN GroupStudent GS on GS.GroupId = GP.GroupId JOIN  Person PR on PR.Id = GS.StudentId ORDER BY PJ.Id;";
            SqlCommand sqlCommand = new SqlCommand(query,con);
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public static DataTable ReportTwoQuery()
        {
            string query = "select p.FirstName+ ' '+ p.LastName as StudentName , pj.Title as PrjectTitle ,e.Name as EvaluationName,ge.ObtainedMarks as ObtainedMarks , e.TotalMarks AS TotalMarks , CONCAT(ROUND(((CAST(ObtainedMarks AS FLOAT) / TotalMarks) * e.TotalWeightage), 2),'%') AS ObtainedWeightage, CONCAT(e.TotalWeightage,'%') AS TotalWeightage from  GroupStudent gs join Person p on gs.StudentId = p.Id join  GroupProject gp on gs.GroupId = gp.GroupId join Project pj  on gp.ProjectId = pj.Id join GroupEvaluation ge on ge.GroupId = gp.GroupId join Evaluation e on ge.EvaluationId = e.Id;";
            SqlCommand sqlCommand = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public static DataTable ReportThreeQuery()
        {
            string query = "SELECT gp.GroupId, p.Title AS ProjectTitle, COUNT(DISTINCT gs.StudentId) AS NoOfStudents, (SUM(ge.ObtainedMarks)/COUNT(DISTINCT gs.StudentId)) AS TotalObtainedMarks, (SUM(e.TotalMarks)/COUNT(DISTINCT gs.StudentId)) AS TotalPossibleMarks FROM GroupStudent gs JOIN [Group] g ON gs.GroupId = g.Id JOIN GroupProject gp ON g.Id = gp.GroupId JOIN Project p ON p.Id = gp.ProjectId LEFT JOIN GroupEvaluation ge ON g.Id = ge.GroupId LEFT JOIN Evaluation e ON ge.EvaluationId = e.Id GROUP BY p.Title, gp.GroupId;";
            SqlCommand sqlCommand = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public static DataTable ReportFourQuery()
        {
            string query = "SELECT TOP(3) gp.GroupId, p.Title AS ProjectTitle, COUNT(DISTINCT gs.StudentId) AS NoOfStudents, (SUM(ge.ObtainedMarks)/COUNT(DISTINCT gs.StudentId)) AS TotalObtainedMarks, (SUM(e.TotalMarks)/COUNT(DISTINCT gs.StudentId)) AS TotalPossibleMarks, CONCAT(ROUND((CAST((SUM(ge.ObtainedMarks)/COUNT(DISTINCT gs.StudentId)) AS FLOAT) / (SUM(e.TotalMarks)/COUNT(DISTINCT gs.StudentId))) * 100 , 2),'%') AS ObtainedPercentage FROM GroupStudent gs JOIN [Group] g ON gs.GroupId = g.Id JOIN GroupProject gp ON g.Id = gp.GroupId JOIN Project p ON p.Id = gp.ProjectId LEFT JOIN GroupEvaluation ge ON g.Id = ge.GroupId LEFT JOIN Evaluation e ON ge.EvaluationId = e.Id GROUP BY p.Title, gp.GroupId ORDER BY ROUND((CAST((SUM(ge.ObtainedMarks)/COUNT(DISTINCT gs.StudentId)) AS FLOAT) / (SUM(e.TotalMarks)/COUNT(DISTINCT gs.StudentId))) * 100 , 2) DESC;";
            SqlCommand sqlCommand = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        public static DataTable ReportFiveQuery()
        {
            string query = "SELECT Lookup.Value AS Gender, COUNT(*) AS TotalStudents, SUM(CASE WHEN gs.Status = 3 THEN 1 ELSE 0 END) AS ActiveStudents, SUM(CASE WHEN gs.Status = 4 THEN 1 ELSE 0 END) AS InactiveStudents, CONCAT(SUM(CASE WHEN gs.Status = 3 THEN 1 ELSE 0 END) * 100 / COUNT(*) ,'%') AS ParticipationRate FROM GroupStudent gs JOIN Person p ON gs.StudentId = p.Id JOIN Lookup ON p.Gender = Lookup.Id GROUP BY Lookup.Value;";
            SqlCommand sqlCommand = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        


    }
}
